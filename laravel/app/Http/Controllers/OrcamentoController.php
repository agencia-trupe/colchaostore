<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\OrcamentosRequest;

use App\Models\ProdutoMedida;
use App\Models\Frete;
use App\Models\Orcamento;
use App\Models\Contato;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;

class OrcamentoController extends Controller
{
    public function index()
    {
        $medidas    = $this->medidasOrcamento();
        $valorTotal = $this->valorTotal($medidas);

        return view('frontend.orcamento', compact('medidas', 'valorTotal'));
    }

    public function store()
    {
        $medida = ProdutoMedida::find(request('medida'));

        if (! $medida) return back();

        $orcamento = session('orcamento', []);

        if (array_key_exists($medida->id, $orcamento)) {
            $orcamento[$medida->id] += 1;
        } else {
            $orcamento[$medida->id] = 1;
        }

        session()->put('orcamento', $orcamento);

        return redirect()->route('orcamento');
    }

    public function update()
    {
        $this->validate(request(), ['quantidade' => 'regex:/[0-9]+/']);

        $medida = ProdutoMedida::find(request('medida'));

        if (! $medida) return back();

        $orcamento = session('orcamento', []);

        if (array_key_exists($medida->id, $orcamento) && $quantidade = request('quantidade')) {
            $orcamento[$medida->id] = (int)$quantidade;
        }

        if ($quantidade == 0) {
            unset($orcamento[$medida->id]);
        }

        session()->put('orcamento', $orcamento);

        return redirect()->route('orcamento');
    }

    public function destroy()
    {
        $medida = ProdutoMedida::find(request('medida'));

        if (! $medida) return back();

        $orcamento = session()->get('orcamento', []);

        if (array_key_exists($medida->id, $orcamento)) {
            unset($orcamento[$medida->id]);
        }

        session()->put('orcamento', $orcamento);

        return redirect()->route('orcamento');
    }

    public function frete()
    {
        $this->validate(request(), ['cep' => 'regex:/[0-9]+/']);

        return $this->calculaFrete($this->medidasOrcamento(), request('cep'));
    }

    public function post(OrcamentosRequest $request, Orcamento $orcamento)
    {
        $data = $request->all();

        $medidas = $this->medidasOrcamento();
        if (! count($medidas)) return back();

        $valorTotal = dinheiro($this->valorTotal($medidas));

        $frete = $this->calculaFrete($medidas, request('cep'));
        if (array_key_exists('erro', $frete)) {
            $valorFrete = 'CEP não é coberto pelo sistema de frete';
            $total = $valorTotal;
        } else {
            $valorFrete = $frete['frete'];
            $total = $frete['total'];
        }

        $orcamentoHtml = '';
        foreach($medidas as $medida) {
            $unitario = dinheiro($medida->valorParaCalculo());
            $subtotal = dinheiro($medida->valorParaCalculo() * $medida->quantidade);

            $orcamentoHtml .= "
                <p>
                    ".($medida->produto->codigo ? "(código: {$medida->produto->codigo})<br>" : "")."
                    {$medida->produto->titulo} - {$medida->titulo}<br>
                    Quantidade: {$medida->quantidade} unidade(s)<br>
                    Preço Unitário: {$unitario}<br>
                    Subtotal: {$subtotal}
                </p>";
        }

        $orcamentoHtml .= "
            <hr>
            <p>
                Total Orçado: {$valorTotal}<br>
                Frete: {$valorFrete}<br>
                <strong>Total: {$total}</strong>
            </p>
        ";

        $data['orcamento'] = trim(preg_replace('/\s+/', ' ', $orcamentoHtml));
        $model = $orcamento->create($data);
        $data['id'] = $model->id;
        $this->sendMail($data);

        session()->forget('orcamento');

        return back()->with('success', true);
    }

    private function sendMail($data)
    {
        if (! $email = Contato::first()->email) {
           return false;
        }

        Mail::send('emails.orcamento', $data, function($m) use ($email, $data)
        {
            $m->to($email, config('app.name'))
               ->subject('[ORÇAMENTO] '.config('app.name'))
               ->replyTo($data['email'], $data['nome']);
        });
    }

    protected function medidasOrcamento()
    {
        $orcamento = session('orcamento', []);

        if (! count($orcamento)) return collect([]);

        $medidas = ProdutoMedida::with('produto')
            ->whereIn('id', array_keys($orcamento))
            ->orderBy(\DB::raw('FIELD(`id`, ' . implode(
                ',',
                array_keys($orcamento)
            ) . ')'))
            ->get();

        return $medidas->each(function($medida) use ($orcamento) {
            $medida->quantidade = $orcamento[$medida->id];
        });
    }

    protected function valorTotal(Collection $medidas)
    {
        return $medidas->reduce(function($carry, $item) {
            $carry += ($item->valorParaCalculo() * $item->quantidade);
            return $carry;
        }, 0);
    }

    protected function calculaFrete(Collection $medidas, $cep)
    {
        $cep = substr($cep, 0, 5);

        $baseCalculo = Frete::where('cep_de', '<=', $cep)
            ->where('cep_ate', '>=', $cep)
            ->first();

        if (! $baseCalculo) {
            return [
                'erro' => 'O CEP informado não é coberto pelo nosso sistema de frete. Favor solicitar cotação pelo formulário.'
            ];
        }

        $freteTotal = $medidas->map(function($medida) use ($baseCalculo) {
            switch (true) {
                case $medida->cubagem <= 20:
                    $valor_cubagem = $baseCalculo->de0_a_20;
                    break;
                case $medida->cubagem > 20 && $medida->cubagem <= 50:
                    $valor_cubagem = $baseCalculo->de20_a_50;
                    break;
                case $medida->cubagem > 50 && $medida->cubagem <= 100:
                    $valor_cubagem = $baseCalculo->de50_a_100;
                    break;
                case $medida->cubagem > 100 && $medida->cubagem <= 150:
                    $valor_cubagem = $baseCalculo->de100_a_150;
                    break;
                case $medida->cubagem > 150 && $medida->cubagem <= 200:
                    $valor_cubagem = $baseCalculo->de150_a_200;
                    break;
                case $medida->cubagem > 200:
                    $valor_cubagem = $baseCalculo->mais_de_200;
                    break;
            }

            $valor   = $medida->valorParaCalculo() / 100;
            $ad      = ($valor / 100) * $baseCalculo->ad;
            $pedagio = ($medida->peso / 100) * $baseCalculo->pedagio;
            $taxas   = $baseCalculo->taxas_diversas;
            $gris    = ($valor / 100) * $baseCalculo->gris;
            $coleta  = $baseCalculo->coleta;
            $divisao = $baseCalculo->divisao == 0 ? 1 : $baseCalculo->divisao;

            $total_frete   = $valor_cubagem + $ad + $gris + $pedagio + $taxas;
            $total_divisao = $total_frete / $divisao;
            $total_frete   = $total_divisao + $coleta;
            $total_frete   = $total_frete / $divisao;

            if ($ad == 0 && $pedagio == 0 && $gris == 0) {
                $total_frete = $coleta;
            }

            return $total_frete * $medida->quantidade;
        })->reduce(function($carry, $item) {
            return $carry += $item;
        }, 0);

        if ($freteTotal == 0 || $baseCalculo->valor_isento > 0 && $baseCalculo->valor_isento > $freteTotal) {
            $freteTotal = 0;
            $valorFrete = 'Grátis';
        } else {
            $freteTotal = $freteTotal * 100;
            $valorFrete = dinheiro($freteTotal);
        }

        $valorProdutos = $this->valorTotal($medidas);

        if ($valorProdutos > 0) {
            $valorTotal = dinheiro($valorProdutos + $freteTotal);
        } else {
            $valorTotal = dinheiro(0);
        }

        return [
            'frete' => $valorFrete,
            'total' => $valorTotal
        ];
    }
}
