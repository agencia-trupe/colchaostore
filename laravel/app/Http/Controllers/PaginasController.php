<?php

namespace App\Http\Controllers;

use App\Models\Pagina;

class PaginasController extends Controller
{
    public function index(Pagina $pagina)
    {
        return view('frontend.pagina', compact('pagina'));
    }
}
