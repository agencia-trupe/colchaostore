<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ProdutoDestaque;

class ProdutosDestaquesController extends Controller
{
    public function index()
    {
        $registros = ProdutoDestaque::with('produto')->ordenados()->get();

        return view('painel.produtos.destaques.index', compact('registros'));
    }

    public function destroy(ProdutoDestaque $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.destaques.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
