<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\ProdutosRelacionadosRequest;

use App\Models\Produto;
use App\Models\ProdutoRelacionado;

class ProdutosRelacionadosController extends Controller
{
    public function index(Produto $produto)
    {
        $registros = $produto->relacionados;

        return view('painel.produtos.relacionados.index', compact('produto', 'registros'));
    }

    public function create(Produto $produto)
    {
        $relacionados = $produto->relacionados
            ->pluck('relacionado_id')->push($produto->id);

        $produtos = Produto::
            whereNotIn('id', $relacionados)
            ->orderBy('id', 'DESC')
            ->get()->lists('titulo', 'id');

        return view('painel.produtos.relacionados.create', compact('produtos', 'produto'));
    }

    public function store(Produto $produto, ProdutosRelacionadosRequest $request)
    {
        try {

            $input = $request->except('back_to');

            if ($input['relacionado_id'] == $produto->id) {
                throw new \Exception('O produto relacionado não pode ser igual ao próprio produto.');
            }

            if (ProdutoRelacionado::where('produto_id', $produto->id)->where('relacionado_id', $input['relacionado_id'])->count()) {
                throw new \Exception('O produto selecionado já foi adicionado.');
            }

            $produto->relacionados()->create($input);

            return redirect()->route('painel.produtos.relacionados.index', [$produto, 'back_to' => $request->back_to])->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Request $request, Produto $produto, ProdutoRelacionado $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.relacionados.index', [$produto, 'back_to' => $request->back_to])->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
