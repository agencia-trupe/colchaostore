<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosCategoriasRequest;
use App\Http\Controllers\Controller;

use App\Models\ProdutoCategoria;
use App\Models\ProdutoSecao;

class ProdutosCategoriasController extends Controller
{
    public function index(ProdutoSecao $secao)
    {
        $registros = ProdutoCategoria::where('secao_id', $secao->id)
            ->ordenados()->get();

        return view('painel.produtos.categorias.index', compact('registros', 'secao'));
    }

    public function create(ProdutoSecao $secao)
    {
        return view('painel.produtos.categorias.create', compact('secao'));
    }

    public function store(ProdutoSecao $secao, ProdutosCategoriasRequest $request)
    {
        try {

            $input = $request->except('back_to');
            $input['secao_id'] = $secao->id;

            ProdutoCategoria::create($input);

            return redirect()->route('painel.produtos.{secoes}.categorias.index', [$secao, 'back_to' => $request->back_to])->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(ProdutoSecao $secao, ProdutoCategoria $registro)
    {
        return view('painel.produtos.categorias.edit', compact('secao', 'registro'));
    }

    public function update(ProdutoSecao $secao, ProdutoCategoria $registro, ProdutosCategoriasRequest $request)
    {
        try {

            $input = $request->except('back_to');

            $registro->update($input);

            return redirect()->route('painel.produtos.{secoes}.categorias.index', [$secao, 'back_to' => $request->back_to])->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Request $request, ProdutoSecao $secao, ProdutoCategoria $registro)
    {
        try {

            $registro->subcategorias()->delete();
            $registro->delete();

            return redirect()->route('painel.produtos.{secoes}.categorias.index', [$secao, 'back_to' => $request->back_to])->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
