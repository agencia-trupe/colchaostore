<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Produto;
use App\Models\ProdutoImagem;

use App\Helpers\CropImage;

class ProdutosImagensController extends Controller
{
    public function index(Produto $registro)
    {
        $imagens = ProdutoImagem::produto($registro->id)->ordenados()->get();

        return view('painel.produtos.imagens.index', compact('imagens', 'registro'));
    }

    public function show(Produto $registro, ProdutoImagem $imagem)
    {
        return $imagem;
    }

    public function store(Produto $registro, ProdutosImagensRequest $request)
    {
        try {

            $input = $request->except('back_to');
            $input['imagem'] = ProdutoImagem::uploadImagem();
            $input['produto_id'] = $registro->id;

            $imagem = ProdutoImagem::create($input);

            $back_to = $request->back_to;

            $view = view('painel.produtos.imagens.imagem', compact('registro', 'imagem', 'back_to'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Request $request, Produto $registro, ProdutoImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.produtos.imagens.index', [$registro, 'back_to' => $request->back_to])
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Request $request, Produto $registro)
    {
        try {

            $registro->imagens()->delete();
            return redirect()->route('painel.produtos.imagens.index', [$registro, 'back_to' => $request->back_to])
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
