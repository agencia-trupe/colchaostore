<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosRequest;
use App\Http\Controllers\Controller;

use App\Models\Produto;
use App\Models\ProdutoSecao;
use App\Models\ProdutoCategoria;
use App\Models\ProdutoMarca;
use App\Models\ProdutoPropriedade;
use App\Models\ProdutoImagem;

class ProdutosController extends Controller
{
    public function index(ProdutoSecao $secao)
    {
        $marcas = ProdutoMarca::ordenados()->lists('nome', 'id');
        $categorias = ProdutoCategoria::where('secao_id', $secao->id)->ordenados()->lists('titulo', 'id');

        $registros = Produto::secao($secao)
            ->with('categoria', 'subcategoria', 'marca');

        if ($marca = request('marca')) {
            $registros = $registros->where('marca_id', $marca);
        }
        if ($categoria = request('categoria')) {
            $registros = $registros->where('categoria_id', $categoria);
        }
        if ($subcategoria = request('subcategoria')) {
            $registros = $registros->where('subcategoria_id', $subcategoria);
        }

        $registros = $registros->ordenados()->get();

        return view('painel.produtos.index', compact('secao', 'marcas', 'categorias', 'registros'));
    }

    public function create(ProdutoSecao $secao) {
        $categorias = ProdutoCategoria::where('secao_id', $secao->id)->ordenados()->lists('titulo', 'id');
        $marcas = ProdutoMarca::ordenados()->lists('nome', 'id');
        $propriedades = ProdutoPropriedade::ordenados()->get();

        return view('painel.produtos.create', compact('secao', 'categorias', 'marcas', 'propriedades'));
    }

    public function store(ProdutoSecao $secao, ProdutosRequest $request)
    {
        try {

            $input = $request->except(['back_to', 'propriedades', 'destaque', 'promocao']);
            $input['secao_id'] = $secao->id;
            $input['capa'] = Produto::upload_capa();

            if (request('subcategoria_id') === '') {
                $input['subcategoria_id'] = null;
            }

            $registro = Produto::create($input);
            $registro->propriedades()->sync(request('propriedades') ?: []);
            $registro->marcaDestaque($request->get('destaque'));
            $registro->marcaPromocao($request->get('promocao'));

            $redirect = $request->back_to
                ? redirect($request->back_to)
                : redirect()->route('painel.produtos.{secoes}.index', $secao);

            return $redirect->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(ProdutoSecao $secao, Produto $registro) {
        $categorias = ProdutoCategoria::where('secao_id', $secao->id)->ordenados()->lists('titulo', 'id');
        $marcas = ProdutoMarca::ordenados()->lists('nome', 'id');
        $propriedades = ProdutoPropriedade::ordenados()->get();

        return view('painel.produtos.edit', compact('secao', 'registro', 'categorias', 'marcas', 'propriedades'));
    }

    public function update(ProdutoSecao $secao, Produto $registro, ProdutosRequest $request)
    {
        try {

            $input = $request->except(['back_to', 'propriedades', 'destaque', 'promocao']);

            if (isset($input['capa'])) $input['capa'] = Produto::upload_capa();

            if (request('subcategoria_id') === '') {
                $input['subcategoria_id'] = null;
            }

            $registro->update($input);
            $registro->propriedades()->sync(request('propriedades') ?: []);
            $registro->marcaDestaque($request->get('destaque'));
            $registro->marcaPromocao($request->get('promocao'));

            $redirect = $request->back_to
                ? redirect($request->back_to)
                : redirect()->route('painel.produtos.{secoes}.index', $secao);

            return $redirect->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Request $request, ProdutoSecao $secao, Produto $registro) {
        try {

            $registro->delete();

            $redirect = $request->back_to
                ? redirect($request->back_to)
                : redirect()->route('painel.produtos.{secoes}.index', $secao);

            return $redirect->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
