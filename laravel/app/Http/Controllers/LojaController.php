<?php

namespace App\Http\Controllers;

use App\Models\Contato;

class LojaController extends Controller
{
    public function index()
    {
        $contato = Contato::first();

        return view('frontend.loja', compact('contato'));
    }
}
