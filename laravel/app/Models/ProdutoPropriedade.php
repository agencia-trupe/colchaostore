<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ProdutoPropriedade extends Model
{
    protected $table = 'produtos_propriedades';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 60,
            'height' => 60,
            'path'   => 'assets/img/produtos/propriedades/'
        ]);
    }
}
