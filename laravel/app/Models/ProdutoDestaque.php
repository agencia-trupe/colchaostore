<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdutoDestaque extends Model
{
    protected $table = 'produtos_destaques';

    protected $guarded = ['id'];

    public function produto()
    {
        return $this->belongsTo(Produto::class, 'produto_id');
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
}
