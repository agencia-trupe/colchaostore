<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pagina extends Model
{
    protected $table = 'paginas';

    protected $guarded = ['id', 'titulo', 'slug'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function imagens()
    {
        return $this->hasMany(PaginaImagem::class, 'pagina_id')->ordenados();
    }
}
