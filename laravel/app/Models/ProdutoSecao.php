<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\CropImage;

class ProdutoSecao extends Model
{
    protected $table = 'produtos_secoes';

    protected $guarded = ['id', 'titulo', 'slug'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'       => 475,
            'height'      => 255,
            'path'        => 'assets/img/produtos/destaque-secao/'
        ]);
    }

    public function categorias()
    {
        return $this->hasMany(ProdutoCategoria::class, 'secao_id')->ordenados();
    }

    public function produto()
    {
        return $this->belongsTo(Produto::class, 'produto_id');
    }
}
