<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Curriculo extends Model
{
    protected $table = 'curriculos';

    protected $guarded = ['id'];

    public function scopeNaoLidos($query)
    {
        return $query->where('lido', false);
    }

    public function marcaComoLido()
    {
        return $this->update(['lido' => true]);
    }
}
