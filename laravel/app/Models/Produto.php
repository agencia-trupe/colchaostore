<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Produto extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'produtos';

    protected $guarded = ['id'];

    const TIPOS_CONFORTO = [
        1 => 'Firme',
        2 => 'Intermediário',
        3 => 'Macio'
    ];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeOrdenadosSecao($query)
    {
        return $query
            ->join('produtos_secoes as secoes', 'produtos.secao_id', '=', 'secoes.id')
            ->join('produtos_categorias as categorias', 'produtos.categoria_id', '=', 'categorias.id')
            ->orderBy('secoes.id', 'ASC')
            ->orderBy('categorias.ordem', 'ASC')
            ->orderBy('categorias.id', 'DESC')
            ->orderBy('produtos.ordem', 'ASC')
            ->orderBy('produtos.id', 'DESC')
            ->select('produtos.*');
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\ProdutoImagem', 'produto_id')->ordenados();
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            [
                'width'  => 260,
                'height' => 260,
                'color'  => '#fff',
                'path'   => 'assets/img/produtos/capa/'
            ],
            [
                'width'  => 125,
                'height' => 110,
                'color'  => '#fff',
                'path'   => 'assets/img/produtos/imagens/thumbs/'
            ],
            [
                'width'  => 530,
                'height' => 450,
                'color'  => '#fff',
                'path'   => 'assets/img/produtos/imagens/'
            ],
            [
                'width'  => 1060,
                'height' => 900,
                'color'  => '#fff',
                'path'   => 'assets/img/produtos/imagens/zoom/'
            ]
        ]);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function secaoModel()
    {
        return $this->belongsTo(ProdutoSecao::class, 'secao_id');
    }

    public function scopeSecao($query, ProdutoSecao $secao)
    {
        return $query->where('secao_id', $secao->id);
    }

    public function scopeBusca($query, $termo)
    {
        return $query
            ->where('titulo', 'LIKE', "%{$termo}%")
            ->orWhere('material', 'LIKE', "%{$termo}%")
            ->orWhere('linha', 'LIKE', "%{$termo}%")
            ->orWhere('legenda', 'LIKE', "%{$termo}%")
            ->orWhere('codigo', 'LIKE', "%{$termo}%")
            ->orWhere('resumo', 'LIKE', "%{$termo}%")
            ->orWhere('descricao', 'LIKE', "%{$termo}%")
            ->orWhere('dados_complementares', 'LIKE', "%{$termo}%");
    }

    public function destaque()
    {
        return $this->hasOne(ProdutoDestaque::class);
    }

    public function marcaDestaque($checkbox)
    {
        if ($checkbox && is_null($this->destaque)) {
            $this->destaque()->create([]);
        }

        if (! $checkbox && $this->destaque) {
            $this->destaque->delete();
        }
    }

    public function promocao()
    {
        return $this->hasOne(ProdutoPromocao::class);
    }

    public function marcaPromocao($checkbox)
    {
        if ($checkbox && is_null($this->promocao)) {
            $this->promocao()->create([]);
        }

        if (! $checkbox && $this->promocao) {
            $this->promocao->delete();
        }
    }

    public function categoria()
    {
        return $this->belongsTo(ProdutoCategoria::class, 'categoria_id');
    }

    public function subcategoria()
    {
        return $this->belongsTo(ProdutoSubcategoria::class, 'subcategoria_id');
    }

    public function marca()
    {
        return $this->belongsTo(ProdutoMarca::class, 'marca_id');
    }

    public function propriedades()
    {
        return $this->belongsToMany(ProdutoPropriedade::class, 'produto_propriedade', 'produto_id', 'propriedade_id');
    }

    public function medidas()
    {
        return $this->hasMany(ProdutoMedida::class, 'produto_id')->ordenados();
    }

    public function relacionados()
    {
        return $this->hasMany(ProdutoRelacionado::class, 'produto_id')->ordenados();
    }

    private function valoresOrdenados()
    {
        if (! count($medidas = $this->medidas)) return [];

        $valores = [];

        foreach($medidas as $medida) {
            foreach(['a_vista', 'oferta', 'parcelado'] as $campo) {
                if ($medida->{$campo} > 0) {
                    $valores[] = $medida->{$campo};
                }
            }
        }

        sort($valores);

        return $valores;
    }

    public function menorValor()
    {
        $valoresOrdenados = $this->valoresOrdenados();
        return array_shift($valoresOrdenados);
    }

    public function maiorValor()
    {
        $valoresOrdenados = $this->valoresOrdenados();
        return array_pop($valoresOrdenados);
    }
}
