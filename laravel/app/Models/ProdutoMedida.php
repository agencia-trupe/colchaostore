<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdutoMedida extends Model
{
    protected $table = 'produtos_medidas';

    protected $guarded = ['id'];

    public function produto()
    {
        return $this->belongsTo(Produto::class, 'produto_id');
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function setAVistaAttribute($value)
    {
        $this->attributes['a_vista'] = preg_replace('/[^0-9]/', '', $value);
    }

    public function setOfertaAttribute($value)
    {
        $this->attributes['oferta'] = preg_replace('/[^0-9]/', '', $value);
    }

    public function setParceladoAttribute($value)
    {
        $this->attributes['parcelado'] = preg_replace('/[^0-9]/', '', $value);
    }

    public function parcelas()
    {
        $parcelas = [];

        foreach(range(1, $this->parcelas) as $parcela)
        {
            $parcelas[$parcela] = $this->parcelado / $parcela;
        }

        return $parcelas;
    }

    public function valorParaCalculo()
    {
        if ($this->oferta) return $this->oferta;
        if ($this->a_vista) return $this->a_vista;
        if ($this->parcelado) return $this->parcelado;
        return 0;
    }
}
