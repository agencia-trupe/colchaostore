@extends('frontend.common.template')

@section('breadcrumb')
    Todas as marcas
    @if($marca)
    <span>></span>
    {{ $marca->nome }}
    @endif
@endsection

@section('content')

    <div class="produtos-sidebar center">
        <div class="produtos-filters">
            <div class="filters-desktop">
                <div class="filters-header">
                    <a href="{{ route('produtos.marcas') }}">
                        Todas as marcas
                    </a>
                </div>
                <div class="filter-links">
                    @foreach($marcas as $m)
                    <a href="{{ route('produtos.marcas', $m) }}" @if($marca && $marca->id === $m->id) class="active" @endif>
                        {{ $m->nome }}
                    </a>
                    @endforeach
                </div>
            </div>
            <div class="filters-mobile">
                <select name="filtro_marcas" class="filtro-select">
                    <option value="{{ route('produtos.marcas') }}" @if(!$marca) selected @endif>Todas as marcas</option>
                    @foreach($marcas as $m)
                    <option value="{{ route('produtos.marcas', $m) }}" @if($marca && $marca->id === $m->id) selected @endif>{{ $m->nome }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="produtos-list produtos-list-side">
            @if(count($produtos))
                @include('frontend.produtos._thumbs', compact('produtos'))
            @else
                <div class="produtos-nenhum">Nenhum produto encontrado.</div>
            @endif
        </div>

        @if(count($produtos) && $produtos->hasMorePages())
        {!! $produtos->links() !!}
        <div class="load-next-wrapper">
            <div class="load-next">CARREGAR MAIS PRODUTOS</div>
        </div>
        @endif
    </div>

@endsection
