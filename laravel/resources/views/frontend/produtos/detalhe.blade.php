@extends('frontend.common.template')

@section('breadcrumb')

    <a href="{{ route('produtos.secao', $produto->secaoModel) }}">
        {{ $produto->secaoModel->titulo }}
    </a>
    <span>></span>
    <a href="{{ route('produtos.categoria', [$produto->secaoModel, $produto->categoria]) }}">
        {{ $produto->categoria->titulo }}
    </a>
    @if($produto->subcategoria)
    <span>></span>
    <a href="{{ route('produtos.subcategoria', [$produto->secaoModel, $produto->categoria, $produto->subcategoria]) }}">
        {{ $produto->subcategoria->titulo }}
    </a>
    @endif

@endsection

@section('content')

    <div class="produtos-detalhe center">
        <div class="textos">
            <h1>{{ $produto->titulo }}</h1>
            @if($produto->codigo)
            <p class="codigo">{{ $produto->codigo }}</p>
            @endif
            <p class="resumo">{{ $produto->resumo }}</p>

            @if(count($produto->medidas))
            <div class="medidas-valores">
                <div class="nav-medidas">
                    @foreach($produto->medidas as $medida)
                    <a href="#" data-id="{{ $medida->id }}" @if($produto->medidas->first() == $medida) class="active" @endif>
                        {!! $medida->titulo !!}
                    </a>
                    @endforeach
                    <select name="medida" class="medida-select">
                        @foreach($produto->medidas as $medida)
                        <option value="{{ $medida->id }}" @if($produto->medidas->first() == $medida) selected @endif>
                            {{ strip_tags($medida->titulo) }}
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="detalhes-medida">
                    @foreach($produto->medidas as $medida)
                    <div class="medida @if($produto->medidas->first() == $medida) active @endif" data-id="{{ $medida->id }}">
                        <div class="medida-a-vista">
                            à vista no boleto:
                            @if($medida->oferta)
                            <span class="de">De: <span>{{ dinheiro($medida->a_vista) }}</span></span>
                            <span class="por">Por: <strong>{{ dinheiro($medida->oferta) }}</strong></span>
                            <span class="oferta">OFERTA</span>
                            @else
                            <span class="por"><strong>{{ dinheiro($medida->a_vista) }}</strong></span>
                            @endif
                        </div>
                        @if($medida->parcelado)
                        <div class="medida-parcelado">
                            <p>
                                @if($medida->parcelas > 1)
                                    parcelado no Cartão de Crédito<br>
                                    (em até {{ $medida->parcelas }}x sem juros):
                                @else
                                    Cartão de Crédito:
                                @endif

                                @foreach($medida->parcelas() as $parcelas => $valor)
                                <span>{{ $parcelas }}x de {{ dinheiro($valor) }}</span>
                                @endforeach
                            </p>
                        </div>
                        @endif
                    </div>
                    @endforeach
                    <form action="{{ route('orcamento.store') }}" method="POST" class="form-adiciona-produto">
                        {!! csrf_field() !!}
                        <input type="hidden" name="medida" value="{{ $produto->medidas->first()->id }}">
                        <input type="submit" value="INSERIR NO ORÇAMENTO">
                    </form>
                </div>
            </div>
            @endif

            <div class="descricao">
                <h3>DESCRIÇÃO DO PRODUTO</h3>
                {!! $produto->descricao !!}
            </div>

            @if($produto->dados_complementares)
            <div class="descricao">
                <h3>DADOS COMPLEMENTARES</h3>
                {!! $produto->dados_complementares !!}
            </div>
            @endif
        </div>

        <div class="imagens-propriedades">
            <div class="imagens">
                <div class="imagem-aberta">
                    <div class="imagem-zoom" data-zoom="{{ asset('assets/img/produtos/imagens/zoom/'.$produto->capa) }}">
                        <img src="{{ asset('assets/img/produtos/imagens/'.$produto->capa) }}" alt="">
                    </div>

                    @if(count($produto->imagens))
                    <a href="#" class="imagens-nav imagens-nav-prev"></a>
                    <a href="#" class="imagens-nav imagens-nav-next"></a>
                    @endif
                </div>
                @if(count($produto->imagens))
                <div class="imagens-thumbs">
                    <a href="{{ asset('assets/img/produtos/imagens/'.$produto->capa) }}" class="active" data-zoom="{{ asset('assets/img/produtos/imagens/zoom/'.$produto->capa) }}">
                        <img src="{{ asset('assets/img/produtos/imagens/thumbs/'.$produto->capa) }}" alt="">
                    </a>

                    @foreach($produto->imagens as $imagem)
                    <a href="{{ asset('assets/img/produtos/imagens/'.$imagem->imagem) }}" data-zoom="{{ asset('assets/img/produtos/imagens/zoom/'.$imagem->imagem) }}">
                        <img src="{{ asset('assets/img/produtos/imagens/thumbs/'.$imagem->imagem) }}" alt="">
                    </a>
                    @endforeach
                </div>
                @endif
                <div id="image-zoom"></div>
            </div>

            @if(count($produto->propriedades))
            <div class="propriedades">
                <h3>PROPRIEDADES</h3>
                <div class="propriedades-thumbs">
                @foreach($produto->propriedades as $propriedade)
                    <div>
                        <img src="{{ asset('assets/img/produtos/propriedades/'.$propriedade->imagem) }}" alt="">
                        <span>{{ $propriedade->titulo }}</span>
                    </div>
                @endforeach
                </div>
            </div>
            @endif
        </div>
    </div>

    @if(count($produto->relacionados))
    <div class="produtos-relacionados">
        <div class="center">
            <p><span>PRODUTOS RELACIONADOS</span></p>
            <div class="produtos-list produtos-list-relacionados">
                @include('frontend.produtos._thumbs', [
                    'produtos' => $produto->relacionados->map(function($p) {
                        return $p->relacionado;
                    })
                ])
            </div>
        </div>
    </div>
    @endif

@endsection
