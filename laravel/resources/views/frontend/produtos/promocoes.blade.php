@extends('frontend.common.template')

@section('breadcrumb') Promoções @endsection

@section('content')

    <div class="center">
        @if(count($promocoes))
            <div class="produtos-list produtos-list-fullwidth">
                @include('frontend.produtos._thumbs', [
                    'produtos' => $promocoes->map(function($promocao) {
                        return $promocao->produto;
                    })
                ])
            </div>
        @else
            <div class="produtos-nenhum">Nenhum produto encontrado.</div>
        @endif
    </div>

@endsection
