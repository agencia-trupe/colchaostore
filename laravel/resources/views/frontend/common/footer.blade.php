    <footer>
        <div class="center">
            <a href="#" class="btn-topo">TOPO</a>
        </div>
        <div class="center quadros">
            <div class="quadro">
                <div class="links">
                    @foreach($paginas as $pagina)
                        <a href="{{ route('paginas', $pagina) }}">{{ $pagina->titulo }}</a>
                    @endforeach
                    <a href="{{ route('loja') }}">Loja | Localização - mapa</a>
                </div>
            </div>
            <div class="quadro">
                <div class="links">
                    <a href="{{ route('produtos.secao', 'colchoes') }}">Colchões</a>
                    <a href="{{ route('produtos.secao', 'bases') }}">Bases</a>
                    <a href="{{ route('produtos.secao', 'cabeceiras') }}">Cabeceiras</a>
                    <a href="{{ route('produtos.secao', 'sofa-cama-poltronas') }}">Sofá-cama & Poltronas</a>
                </div>
            </div>
            <div class="quadro">
                <div class="links">
                    <a href="{{ route('produtos.secao', 'bicama-dobraveis') }}">Bicama & Dobráveis</a>
                    <a href="{{ route('produtos.secao', 'moveis') }}">Móveis</a>
                    <a href="{{ route('produtos.secao', 'travesseiros-acessorios') }}">Travesseiros & Acessórios</a>
                    <a href="{{ route('produtos.promocoes') }}">Promoções</a>
                </div>
            </div>
            <div class="quadro">
                <div class="social">
                    @if($contato->instagram)
                    <a href="{{ $contato->instagram }}" class="instagram" target="_blank">instagram</a>
                    @endif
                    @if($contato->facebook)
                    <a href="{{ $contato->facebook }}" class="facebook" target="_blank">facebook</a>
                    @endif
                </div>
                <div class="informacoes">
                    <p class="telefone">{{ $contato->telefone }}</p>
                    <p class="email"><a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a></p>
                    <p class="endereco">{{ str_replace('<br />',' &middot; ', $contato->endereco) }}</p>
                    <p class="atendimento">Horário de atendimento: {{ $contato->horario_de_atendimento}}</p>
                </div>
            </div>
        </div>
        <div class="center marca">
            <div class="links">
                <a href="{{ route('trabalhe-conosco') }}">Trabalhe Conosco</a>
                <a href="{{ route('fale-conosco') }}">Fale Conosco</a>
            </div>
            <img src="{{ asset('assets/img/layout/colchaostore-footer.png') }}" alt="">
            <div class="cnpj-whatsapp">
                <p class="cnpj">Empresa do grupo Jabaquara Colchões Ltda.<br>CNPJ 54.596.275/0001-69</p>
                <p class="whatsapp">Atendimento via whatsapp <span>{{ $contato->whatsapp }}</span></p>
                <p class="acessibilidade">
                    <img src="{{ asset('assets/img/layout/acessibilidade.png') }}" alt="">
                    Loja com acessibilidade para deficientes
                </p>
            </div>
        </div>
        <div class="center copyright">
            <p>© {{ date('Y') }} {{ config('app.name') }} - Todos os direitos reservados. <span>|</span> <a href="http://www.trupe.net" target="_blank">Criação de Sites</a>:<a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a></p>
        </div>
    </footer>
