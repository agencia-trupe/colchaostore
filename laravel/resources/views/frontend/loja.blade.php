@extends('frontend.common.template')

@section('breadcrumb') Loja | Localização - mapa @endsection

@section('content')

    <div class="pagina center">
        <div class="texto">
            <h1>LOJA | LOCALIZAÇÃO - MAPA</h1>
            <p>{!! $contato->endereco !!}</p>
        </div>
        <div class="imagens">
            <img src="{{ asset('assets/img/contato/'.$contato->imagem) }}" alt="">
        </div>
    </div>

    <div class="mapa">
        {!! $contato->google_maps !!}
    </div>

@endsection
