@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Frete
            <a href="{{ route('painel.frete.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Registro</a>
        </h2>
    </legend>

    </div>
    <div class="container-fluid" style="max-width:1500px;margin-top:-30px">

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th style="vertical-align: middle">Estado</th>
                <th style="vertical-align: middle">Cidade</th>
                <th style="vertical-align: middle">0 a 20Kg</th>
                <th style="vertical-align: middle">20 a 50Kg</th>
                <th style="vertical-align: middle">50 a 100Kg</th>
                <th style="vertical-align: middle">100 a 150Kg</th>
                <th style="vertical-align: middle">150 a 200Kg</th>
                <th style="vertical-align: middle">Acima de 200Kg</th>
                <th style="vertical-align: middle">% AD Valorem</th>
                <th style="vertical-align: middle">Pedágio</th>
                <th style="vertical-align: middle">% TX Diversas</th>
                <th style="vertical-align: middle">% GRIS</th>
                <th style="vertical-align: middle">CEP de</th>
                <th style="vertical-align: middle">CEP até</th>
                <th style="vertical-align: middle">Valor para frete grátis</th>
                <th style="vertical-align: middle">Taxa de coleta</th>
                <th style="vertical-align: middle">Fator de divisão</th>
                <th style="vertical-align: middle">ICMS</th>
                <th style="vertical-align: middle;text-align:center" class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr>
                <td>{{ $registro->estado }}</td>
                <td>{{ $registro->cidade }}</td>
                <td>{{ $registro->de0_a_20 }}</td>
                <td>{{ $registro->de20_a_50 }}</td>
                <td>{{ $registro->de50_a_100 }}</td>
                <td>{{ $registro->de100_a_150 }}</td>
                <td>{{ $registro->de150_a_200 }}</td>
                <td>{{ $registro->mais_de_200 }}</td>
                <td>{{ $registro->ad }}</td>
                <td>{{ $registro->pedagio }}</td>
                <td>{{ $registro->taxas_diversas }}</td>
                <td>{{ $registro->gris }}</td>
                <td>{{ $registro->cep_de }}</td>
                <td>{{ $registro->cep_ate }}</td>
                <td>{{ $registro->valor_isento }}</td>
                <td>{{ $registro->coleta }}</td>
                <td>{{ $registro->divisao }}</td>
                <td>
                    <span class="glyphicon glyphicon-{{ $registro->icms == 'sim' ? 'ok' : 'remove' }}"></span></td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.frete.destroy', $registro],
                        'method' => 'delete'
                    ]) !!}
                        <div class="btn-group-vertical">
                            <a href="{{ route('painel.frete.edit', $registro) }}" class="btn btn-primary btn-sm pull-left">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                            <button type="submit" class="btn btn-danger btn-sm btn-delete">
                                <span class="glyphicon glyphicon-remove">
                            </button>
                        </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
