@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Frete /</small> Adicionar Registro</h2>
    </legend>

    {!! Form::open(['route' => 'painel.frete.store']) !!}

        @include('painel.frete.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
