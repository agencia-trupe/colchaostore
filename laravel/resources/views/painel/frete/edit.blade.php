@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Frete /</small> Editar Registro</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.frete.update', $registro->id],
        'method' => 'patch'
    ]) !!}

    @include('painel.frete.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
