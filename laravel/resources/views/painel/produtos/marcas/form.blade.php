@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/produtos/marcas/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
        {!! Form::label('home', 'Mostrar na Home') !!}<br>
        <input type="hidden" name="home" value="0">
        @if(!isset($registro))
            <input type="checkbox" name="home" value="1" checked>
        @else
            <input type="checkbox" name="home" value="1" @if($registro->home) checked @endif>
        @endif
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.marcas.index') }}" class="btn btn-default btn-voltar">Voltar</a>
