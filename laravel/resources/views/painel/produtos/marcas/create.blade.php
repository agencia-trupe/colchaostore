@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / Marcas /</small> Adicionar Marca</h2>
    </legend>

    {!! Form::open(['route' => 'painel.produtos.marcas.store', 'files' => true]) !!}

        @include('painel.produtos.marcas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
