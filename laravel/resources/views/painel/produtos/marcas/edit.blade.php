@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / Marcas /</small> Editar Marca</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos.marcas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.produtos.marcas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
