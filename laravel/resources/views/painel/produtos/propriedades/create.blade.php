@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / Propriedades /</small> Adicionar Propriedade</h2>
    </legend>

    {!! Form::open(['route' => 'painel.produtos.propriedades.store', 'files' => true]) !!}

        @include('painel.produtos.propriedades.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
