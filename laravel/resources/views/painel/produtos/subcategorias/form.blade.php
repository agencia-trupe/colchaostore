@include('painel.common.flash')

<input type="hidden" name="back_to" value="{{ request('back_to') }}">

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.{secoes}.categorias.subcategorias.index', [$secao, $categoria, 'back_to' => request('back_to')]) }}" class="btn btn-default btn-voltar">Voltar</a>
