@include('painel.common.flash')

<input type="hidden" name="back_to" value="{{ request('back_to') }}">

<div class="form-group">
    {!! Form::label('relacionado_id', 'Produto Relacionado') !!}
    {!! Form::select('relacionado_id', $produtos, null, ['class' => 'form-group select-filter', 'placeholder' => 'Selecione']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.relacionados.index', [$produto, 'back_to' => request('back_to')]) }}" class="btn btn-default btn-voltar">Voltar</a>
