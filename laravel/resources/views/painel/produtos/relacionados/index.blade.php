@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ request('back_to') ?: route('painel.produtos.{secoes}.index', $produto->secaoModel) }}" class="btn btn-sm btn-default">
        &larr; Voltar para {{ $produto->secaoModel->titulo }}</a>

    <legend>
        <h2>
            <small>Produtos / {{ $produto->secaoModel->titulo }} / {{ $produto->titulo }} /</small>
            Produtos Relacionados
            <a href="{{ route('painel.produtos.relacionados.create', [$produto, 'back_to' => request('back_to')]) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Produto Relacionado</a>
        </h2>
    </legend>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="produtos_relacionados">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Produto Relacionado</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>{{ $registro->relacionado->titulo }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.produtos.relacionados.destroy', $produto, $registro],
                        'method' => 'delete'
                    ]) !!}
                        <input type="hidden" name="back_to" value="{{ request('back_to') }}">
                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
