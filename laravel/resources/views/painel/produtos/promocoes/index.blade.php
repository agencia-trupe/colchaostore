@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2><small>Produtos /</small> Promoções</h2>
    </legend>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum produto marcado como promoção.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="produtos_promocoes">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Produto</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>
                    <a href="{{ route('painel.produtos.{secoes}.edit', [$registro->produto->secaoModel, $registro->produto]) }}">
                        {{ $registro->produto->titulo }}
                    </a>
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.produtos.promocoes.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}
                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
