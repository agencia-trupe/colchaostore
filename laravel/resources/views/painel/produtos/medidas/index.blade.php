@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ request('back_to') ?: route('painel.produtos.{secoes}.index', $produto->secaoModel) }}" class="btn btn-sm btn-default">
        &larr; Voltar para {{ $produto->secaoModel->titulo }}</a>

    <legend>
        <h2>
            <small>Produtos / {{ $produto->secaoModel->titulo }} / {{ $produto->titulo }} /</small>
            Medidas
            <a href="{{ route('painel.produtos.medidas.create', [$produto, 'back_to' => request('back_to')]) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Medida</a>
        </h2>
    </legend>

    @if(count($registros))
    <div class="alert alert-success">
        <small>Valor exibido:</small>
        <strong>A partir de: {{ dinheiro($produto->menorValor()) }}</strong>
    </div>
    @endif

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="produtos_medidas">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Título</th>
                <th>Valor à vista</th>
                <th>Oferta</th>
                <th>Valor parcelado</th>
                <th>Peso</th>
                <th>Cubagem</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>{!! $registro->titulo !!}</td>
                <td>{{ dinheiro($registro->a_vista, '-') }}</td>
                <td>{{ dinheiro($registro->oferta, '-') }}</td>
                <td>
                    @if($registro->parcelado)
                        {{ dinheiro($registro->parcelado) }}
                        <small>({{ $registro->parcelas }} vezes)</small>
                    @else
                        -
                    @endif
                </td>
                <td>{{ $registro->peso }}</td>
                <td>{{ $registro->cubagem }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.produtos.medidas.destroy', $produto, $registro],
                        'method' => 'delete'
                    ]) !!}

                    <input type="hidden" name="back_to" value="{{ request('back_to') }}">

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.produtos.medidas.edit', [$produto, $registro, 'back_to' => request('back_to')]) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
