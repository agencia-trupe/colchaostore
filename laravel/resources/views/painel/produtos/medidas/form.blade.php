@include('painel.common.flash')

<input type="hidden" name="back_to" value="{{ request('back_to') }}">

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::select('medidas', $medidas, null, ['class' => 'form-control medidas-select', 'placeholder' => 'Selecione ou digite abaixo', 'style' => 'margin-bottom:5px']) !!}
    {!! Form::textarea('titulo', null, ['class' => 'form-control ckeditor', 'data-editor' => 'medida']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('a_vista', 'Valor à vista') !!}
            {!! Form::text('a_vista', null, ['class' => 'form-control mascara-dinheiro']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('oferta', 'Oferta') !!}
            {!! Form::text('oferta', null, ['class' => 'form-control mascara-dinheiro']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('parcelado', 'Valor parcelado (Cartão de Crédito)') !!}
            {!! Form::text('parcelado', null, ['class' => 'form-control mascara-dinheiro']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('parcelas', 'Número de parcelas') !!}
            {!! Form::select('parcelas', array_combine(range(1, 10), range(1, 10)), null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('peso', 'Peso') !!}
            {!! Form::number('peso', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('cubagem', 'Cubagem') !!}
            {!! Form::number('cubagem', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>


{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.medidas.index', [$produto, 'back_to' => request('back_to')]) }}" class="btn btn-default btn-voltar">Voltar</a>
