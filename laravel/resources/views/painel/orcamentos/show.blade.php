@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Orçamento #{{ $orcamento->id }}</h2>
    </legend>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $orcamento->created_at }}</div>
    </div>

    <div class="form-group">
        <label>Nome</label>
        <div class="well">{{ $orcamento->nome }}</div>
    </div>

    <div class="form-group">
        <label>E-mail</label>
        <div class="well">
            <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $orcamento->email }}" style="margin-right:5px;border:0;transition:background .3s">
                <span class="glyphicon glyphicon-copy"></span>
            </button>
            {{ $orcamento->email }}
        </div>
    </div>

@if($orcamento->telefone)
    <div class="form-group">
        <label>Telefone</label>
        <div class="well">{{ $orcamento->telefone }}</div>
    </div>
@endif

@if($orcamento->mensagem)
    <div class="form-group">
        <label>Mensagem</label>
        <div class="well">{{ $orcamento->mensagem }}</div>
    </div>
@endif

    <div class="form-group">
        <label>Pedido</label>
        <div class="well well-pedido">{!! $orcamento->orcamento !!}</div>
    </div>

    <style>
        .well-pedido hr { border-color: #ccc; }
        .well-pedido p:last-child { margin-bottom: 0; }
    </style>

    <a href="{{ route('painel.orcamentos.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
