@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>Currículos</h2>
    </legend>

    @if(!count($curriculos))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Telefone</th>
                <th>Cargo pretendido</th>
                <th style="text-align:center">Arquivo</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($curriculos as $curriculo)
            <tr class="tr-row @if(!$curriculo->lido) warning @endif" id="{{ $curriculo->id }}">
                <td>{{ $curriculo->nome }}</td>
                <td>
                    <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $curriculo->email }}" style="margin-right:5px;border:0;transition:background .3s">
                        <span class="glyphicon glyphicon-copy"></span>
                    </button>
                    {{ $curriculo->email }}
                </td>
                <td>{{ $curriculo->telefone }}</td>
                <td>{{ $curriculo->cargo_pretendido }}</td>
                <td style="text-align:center">
                    <a href="{{ route('painel.curriculos.show', $curriculo) }}" class="btn btn-block btn-sm btn-info">
                        <span class="glyphicon glyphicon-file" style="display:block;font-size:1.5em;margin-bottom:6px"></span>
                        Download
                    </a>
                </td>
                <td style="width:100px">
                        {!! Form::open([
                            'route'  => ['painel.curriculos.destroy', $curriculo],
                            'method' => 'delete'
                        ]) !!}
                            <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                        {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $curriculos->links() }}
    @endif

@endsection
