@include('painel.common.flash')

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('email', 'E-mail') !!}
            {!! Form::email('email', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('telefone', 'Telefone') !!}
            {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('whatsapp', 'WhatsApp') !!}
            {!! Form::text('whatsapp', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('endereco', 'Endereço') !!}
            {!! Form::textarea('endereco', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('google_maps', 'Código GoogleMaps') !!}
            {!! Form::text('google_maps', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('horario_de_atendimento', 'Horário de Atendimento') !!}
            {!! Form::text('horario_de_atendimento', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('facebook', 'Facebook') !!}
            {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('instagram', 'Instagram') !!}
            {!! Form::text('instagram', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-12">
        <div class="well form-group">
            {!! Form::label('imagem', 'Imagem') !!}
            @if($contato->imagem)
                <img src="{{ asset('assets/img/contato/'.$contato->imagem) }}" alt="" style="display:block;max-width:100%;margin-bottom:10px">
            @endif
            {!! Form::file('imagem', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
