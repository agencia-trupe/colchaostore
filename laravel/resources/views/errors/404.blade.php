@extends('frontend.common.template', ['errorPage' => true])

@section('content')

    <div class="not-found">
        <h1>Página não encontrada</h1>
    </div>

@endsection
