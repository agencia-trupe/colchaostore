<!DOCTYPE html>
<html>
<head>
    <title>[ORÇAMENTO] {{ config('app.name') }}</title>
    <meta charset="utf-8">
</head>
<body>
    <p><strong>Orçamento #{{ $id }}</strong></p>
    <p>
        <strong>Nome:</strong> {{ $nome }}<br>
        <strong>E-mail:</strong> {{ $email }}<br>
        @if($telefone)
            <strong>Telefone:</strong> {{ $telefone }}<br>
        @endif
        <strong>CEP:</strong> {{ $cep }}
        @if($mensagem)
            <br><strong>Mensagem:</strong> {{ $mensagem }}
        @endif
    </p>
    <hr>
    {!! $orcamento !!}
</body>
</html>
