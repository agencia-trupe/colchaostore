export default function SelecaoCategorias() {
    $('.js-categoria').change(function() {
        var categoriaId = $(this).find('option:selected').val();

        if (categoriaId) {
            var url = $('base').attr('href') + '/painel/fetch-subcategorias/' + categoriaId;

            $.get(url, function(data) {
                $('.js-subcategoria').find('option:not(:first-child)').remove();

                for (var key in data) {
                    $('.js-subcategoria').append($('<option>', {
                        value: key,
                        text: data[key]
                    }));
                }

                $('.js-subcategoria').attr('disabled', false);
            });
        } else {
            $('.js-subcategoria')
                .attr('disabled', true)
                .find('option:not(:first-child)').remove();
        }
    });


    var categoriaId = $('.js-categoria').find('option:selected').val();

    if (categoriaId) {
        var url = $('base').attr('href') + '/painel/fetch-subcategorias/' + categoriaId;

        $.get(url, function(data) {
            for (var key in data) {
                $('.js-subcategoria').append($('<option>', {
                    value: key,
                    text: data[key]
                }));
            }

            $('.js-subcategoria').attr('disabled', false)

            if ($('.js-subcategoria').data('selected')) {
                var subcategoriaId = $('.js-subcategoria').data('selected');

                $('.js-subcategoria')
                    .find('option[value=' + subcategoriaId + ']')
                    .attr('selected', true);
            }
        });
    }
}
