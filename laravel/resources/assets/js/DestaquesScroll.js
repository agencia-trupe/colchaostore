export default function DestaquesScroll() {
    $('.produtos-list').infiniteScroll({
        path: 'a[rel=next]',
        hideNav: '.pagination',
        append: '.produto-thumb',
        history: false,
        scrollThreshold: -50,
        loadOnScroll: false,
        button: '.load-next',
    }).on('append.infiniteScroll', function(event, response, path, items) {
        $(items).hide().fadeIn();
      });
};
