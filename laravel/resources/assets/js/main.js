import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';
import Banners from './Banners';
import DestaquesScroll from './DestaquesScroll';
import CarouselMarcas from './CarouselMarcas';
import Newsletter from './Newsletter';
import FiltroSelect from './FiltroSelect';
import ModalBusca from './ModalBusca';
import Curriculo from './Curriculo';
import ProdutoImagens from './ProdutoImagens';
import ProdutoSelecaoMedidas from './ProdutoSelecaoMedidas';
import FreteOrcamento from './FreteOrcamento';

AjaxSetup();
MobileToggle();
Banners();
DestaquesScroll();
CarouselMarcas();
Newsletter();
FiltroSelect();
ModalBusca();
Curriculo();
ProdutoImagens();
ProdutoSelecaoMedidas();
FreteOrcamento();

$('.btn-topo').click(function(event) {
    event.preventDefault();
    $('html, body').animate({ scrollTop: 0 });
});
