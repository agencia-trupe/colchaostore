export default function ProdutoSelecaoMedidas() {
    $('.nav-medidas a').click(function(event) {
        event.preventDefault();

        if ($(this).hasClass('active')) return;

        $('.nav-medidas a').removeClass('active');
        $('.detalhes-medida .medida').removeClass('active');

        $(this).addClass('active');
        $('.detalhes-medida .medida[data-id='+$(this).data('id')+']')
            .addClass('active');
        $('.medida-select').val($(this).data('id')).change();
        $('.form-adiciona-produto input[name=medida]').val($(this).data('id'));
    });

    $('.medida-select').change(function() {
        $('.nav-medidas a').removeClass('active');
        $('.detalhes-medida .medida').removeClass('active');

        $('.nav-medidas a[data-id='+$(this).val()+']').addClass('active');
        $('.detalhes-medida .medida[data-id='+$(this).val()+']')
            .addClass('active');

        $('.form-adiciona-produto input[name=medida]').val($(this).val());
    });
};
