export default function Newsletter() {
    $('#form-newsletter').submit(function(event) {
        event.preventDefault();

        var $form = $(this),
            $response = $form.find('.response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: 'POST',
            url: $form.attr('action'),
            data: {
                nome: $form.find('input[name=newsletter_nome]').val(),
                email: $form.find('input[name=newsletter_email]').val(),
            }
        }).done(function(data) {
            $response.fadeOut().text(data.message).fadeIn('slow');
            $form[0].reset();
        }).fail(function(data) {
            var res = data.responseJSON,
                txt = res.nome ? res.nome : res.email;
            $response.hide().text(txt).fadeIn('slow');
        }).always(function() {
            $form.removeClass('sending');
        });
    });
};
