<?php

use Illuminate\Database\Seeder;

class PaginasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('paginas')->insert([
            [
                'titulo' => 'Empresa',
                'slug' => 'empresa'
            ],
            [
                'titulo' => 'Como comprar',
                'slug' => 'como-comprar'
            ],
            [
                'titulo' => 'Formas de pagamento',
                'slug' => 'formas-de-pagamento'
            ]
        ]);
    }
}
