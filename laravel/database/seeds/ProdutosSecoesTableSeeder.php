<?php

use Illuminate\Database\Seeder;

class ProdutosSecoesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('produtos_secoes')->insert([
            ['slug' => 'colchoes', 'titulo' => 'Colchões'],
            ['slug' => 'bases', 'titulo' => 'Bases'],
            ['slug' => 'cabeceiras', 'titulo' => 'Cabeceiras'],
            ['slug' => 'sofa-cama-poltronas', 'titulo' => 'Sofá-cama & Poltronas'],
            ['slug' => 'bicama-dobraveis', 'titulo' => 'Bicama & Dobráveis'],
            ['slug' => 'moveis', 'titulo' => 'Móveis'],
            ['slug' => 'travesseiros-acessorios', 'titulo' => 'Travesseiros & Acessórios'],
        ]);
    }
}
